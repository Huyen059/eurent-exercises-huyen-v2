package eurent.tutorial;


import net.democritus.sys.CrudsResult;
import net.democritus.sys.DataRef;
import net.democritus.sys.NullDataRef;
import net.democritus.sys.ParameterContext;
import net.democritus.sys.command.CommandResult;
import net.democritus.sys.command.ICommand;

public class CarFleetLocationResetter {

  public CommandResult perform(ParameterContext<CarDetails> parameter, ICommand command) {
    CarDetails carDetails = parameter.getValue();

    String status = carDetails.getStatus();
    if (!isValidState(status)) {
      return CommandResult.error(command, "Cannot reset a car that is in initial or intermediate state");
    }

    carDetails.setFleetLocation(NullDataRef.EMPTY_DATA_REF);
    carDetails.setStatus(CarState.TO_BE_ASSIGNED.getStatus());

    CarLocalAgent carAgent = CarLocalAgent.getCarAgent(parameter.getUserContext());
    CrudsResult<DataRef> modifyResult = carAgent.modify(carDetails);

    if (modifyResult.isError()) {
      return CommandResult.error(command, "Could not modify car");
    } else {
      return CommandResult.success(command);
    }
  }

  private boolean isValidState(String status) {
    String assigned = CarState.ASSIGNED.getStatus();
    String assignmentFailed = CarState.ASSIGNMENT_FAILED.getStatus();
    return assigned.equals(status) || assignmentFailed.equals(status);
  }

}
