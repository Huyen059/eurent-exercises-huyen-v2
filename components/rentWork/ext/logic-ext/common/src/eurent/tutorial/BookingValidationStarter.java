package eurent.tutorial;

import net.democritus.sys.CrudsResult;
import net.democritus.sys.DataRef;
import net.democritus.sys.ParameterContext;
import net.democritus.sys.command.CommandResult;
import net.democritus.sys.command.ICommand;

public class BookingValidationStarter {

  public CommandResult perform(ParameterContext<BookingDetails> parameter, ICommand command) {
    BookingLocalAgent bookingAgent = BookingLocalAgent.getBookingAgent(parameter.getUserContext());

    BookingDetails booking = parameter.getValue();

    if (!BookingState.CREATED.getStatus().equals(booking.getStatus())) {
      return CommandResult.error(command, "Cannot start validation on booking with status " + booking.getStatus());
    }

    booking.setStatus(BookingState.TO_BE_VALIDATED.getStatus());
    CrudsResult<DataRef> result = bookingAgent.modify(booking);

    if (result.isError()) {
      return CommandResult.error(command, "Modify booking failed");
    } else {
      return CommandResult.success(command);
    }
  }
}
