-- anchor:custom-imports:start
import net.democritus.sys.CrudsResult;
import net.democritus.sys.DataRef;
import net.democritus.sys.Diagnostic;
import net.democritus.sys.SearchResult;
import net.democritus.sys.search.Paging;
import net.democritus.sys.search.SearchDetails;
-- anchor:custom-imports:end
-- anchor:custom-methods:start
  private TaskResult<DataRef> findAvailableFleetLocation(UserContext userContext) {
    FleetLocationLocalAgent fleetLocationAgent = FleetLocationLocalAgent.getFleetLocationAgent(userContext);
    CarLocalAgent carAgent = CarLocalAgent.getCarAgent(userContext);

    SearchResult<FleetLocationInfo> fleetLocSearchResult = fleetLocationAgent.findAllInfos();

    if (fleetLocSearchResult.isError()) {
      return TaskResult.error(fleetLocSearchResult.getDiagnostics());
    }

    for (FleetLocationInfo fleetLocation : fleetLocSearchResult.getResults()) {
      CarFindByFleetLocationEqDetails finder = new CarFindByFleetLocationEqDetails();
      finder.setFleetLocation(fleetLocation.getDataRef());
      SearchDetails<CarFindByFleetLocationEqDetails> searchDetails = doCountOnly(finder);

      SearchResult<DataRef> searchResult = carAgent.find(searchDetails);

      if (fleetLocation.getCapacity() > searchResult.getTotalNumberOfItems()) {
        return TaskResult.success(fleetLocation.getDataRef());
      }
    }

    logger.warn("All fleet locations are full");
    return TaskResult.error();
  }

  private <FINDER> SearchDetails<FINDER> doCountOnly(FINDER finder) {
    Paging paging = new Paging();
    paging.setPage(0);
    paging.setRowsPerPage(0);

    SearchDetails<FINDER> searchDetails = new SearchDetails<FINDER>(finder);
    searchDetails.setPaging(paging);
    searchDetails.setProjection("dataRef");

    return searchDetails;
  }

  private CrudsResult<DataRef> updateFleetLocation(CarDetails carDetails, DataRef fleetLocation, UserContext userContext) {
    CarLocalAgent carAgent = CarLocalAgent.getCarAgent(userContext);
    carDetails.setFleetLocation(fleetLocation);

    return carAgent.modify(carDetails);
  }

-- anchor:custom-methods:end
-- anchor:custom-perform:start
      CarDetails carDetails = targetParameter.getValue();

      TaskResult<DataRef> findFleetResult = findAvailableFleetLocation(userContext);

      if (findFleetResult.isError()) {
        return TaskResult.error(findFleetResult.getDiagnostics());
      }

      DataRef fleetLocation = findFleetResult.getValue();
      CrudsResult<DataRef> modifyResult = updateFleetLocation(carDetails, fleetLocation, userContext);

      if (modifyResult.isError()) {
        return TaskResult.error(modifyResult.getDiagnostics());
      }
-- anchor:custom-perform:end
